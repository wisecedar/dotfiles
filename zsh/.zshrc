# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="spaceship"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="%Y-%m-%dT%T"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source ~/.config/zsh/catppuccin_frappe-zsh-syntax-highlighting.zsh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#
#

YEAR=$(date +%Y)
TODAY=$(date +"%Y-%m-%d")
export PNPM_HOME="/home/nikki/.local/share/pnpm"
export EDITOR=/usr/bin/vim
export LEDGER_FILE="$HOME/Documents/areas/finances/hledger/$YEAR.ledger"

# ALIASES
# cd directly into ledger's directory and launch vim
# alias cdf="kitty -- sh -c 'watch hledger bs -Vse tomorrow' & vim $LEDGER_FILE"
alias cdf="cd $HOME/Documents/areas/finances/hledger && vim $YEAR.ledger"
# open the action-list file
alias ls='ls -alh --color=auto --group-directories-first'
alias python='python3'

# pnpm
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end

# yt-dlp
alias ytd='yt-dlp --config-location ~/.config/yt-dlp/config'

# fnm
export PATH="/home/nikki/.local/share/fnm:$PATH"
eval "`fnm env`"


# FUNCTIONS
#
# SSH into a WP Engine site, or execute a command via SSH if an argument is provided.
function wpessh() {
  if [[ "$2" != "" ]]; then
    ssh $1@$1.ssh.wpengine.net "$2"
  else
    ssh $1@$1.ssh.wpengine.net
  fi
}

precmd() {
  echo "cd $(pwd)" > ~/.config/kitty/kitty-startup.session
}
# export PROMPT_COMMAND="pwd > /tmp/whereami"

# Write something
wr() {
    WRITING_DIR="$HOME/Documents/code/nikki.lol/src/content/writings"
    # mkdir -p $WRITING_DIR

    if [ $# -eq 0 ]; then
        TITLE="REPLACE ME, SILLY GIRL!"
        FILENAME=$WRITING_DIR/replace-me-silly-girl.md
    else
        TITLE="$*"
        FILENAME="${TITLE// /-}"
        FNAME_DIR=$(echo "$FILENAME" | tr '[:upper:]' '[:lower:]' | sed 's/[^a-zA-Z0-9-]//g')
        URL="http://localhost:5173/$FNAME_DIR"
        mkdir -p $WRITING_DIR/$FNAME_DIR
        FILENAME=$WRITING_DIR/$FNAME_DIR/index.md
    fi

    if [[ ! -e $FILENAME ]]; then
        touch $FILENAME
        echo "---" >> $FILENAME
        echo "title: $TITLE" >> $FILENAME
        echo "date: $TODAY" >> $FILENAME
        echo "updated: $TODAY" >> $FILENAME
        echo "draft: true" >> $FILENAME
        echo "lede: " >> $FILENAME
        echo "# Available options: draft, progress, complete" >> $FILENAME
        echo "stage: draft" >> $FILENAME
        echo "epistemicStatus: Initial thoughts." >> $FILENAME
        echo "epistemicTag: emotional" >> $FILENAME
        echo "location: Chesterfield, MA" >> $FILENAME
        echo "tags:" >> $FILENAME
        echo "    - tag" >> $FILENAME
        echo "    - tag" >> $FILENAME
        echo "---" >> $FILENAME
        echo "" >> $FILENAME
        echo "# $TITLE" >> $FILENAME
    fi

    vim $FILENAME
}
