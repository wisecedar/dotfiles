# kitty

My default terminal. [Installation instructions](https://sw.kovidgoyal.net/kitty/binary/). Run the following:

```
$ mkdir -p ~/.config/kitty
$ cp current-theme.conf kitty-startup.session kitty.conf ~/.config/kitty/
```
